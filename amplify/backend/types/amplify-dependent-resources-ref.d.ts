export type AmplifyDependentResourcesAttributes = {
  "auth": {
    "govcoamplify1178dcd7": {
      "AppClientID": "string",
      "AppClientIDWeb": "string",
      "IdentityPoolId": "string",
      "IdentityPoolName": "string",
      "UserPoolArn": "string",
      "UserPoolId": "string",
      "UserPoolName": "string"
    }
  }
}