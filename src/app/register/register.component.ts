import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import Auth from '@aws-amplify/auth';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  registerForm: FormGroup;
  registerError: string;

  constructor(private formBuilder: FormBuilder) {
    this.registerError = '';
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    });
  }

  async register() {
    try {
      const { username, password, email } = this.registerForm.value;
      await Auth.signUp({ username, password,  attributes: {
        email: email,
        birthdate: '10-11-1989',
        phone_number: '+573108369679',
    }});
      // Registration successful, handle the confirmation step
    } catch (error) {
      this.registerError = "Error en regsitro";
    }
  }
}
