import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import Auth from '@aws-amplify/auth';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
   Auth.configure({
      "aws_project_region": "us-east-1",
      "aws_cognito_identity_pool_id": "us-east-1:cbb5a6b0-f325-462f-8074-c896a1821727",
      "aws_cognito_region": "us-east-1",
      "aws_user_pools_id": "us-east-1_n80iAZE3M",
      "aws_user_pools_web_client_id": "4ru8dhsdk2ou248dp4vr0ki5jf",
      "oauth": {},
      "aws_cognito_username_attributes": [],
      "aws_cognito_social_providers": [],
      "aws_cognito_signup_attributes": [
          "EMAIL"
      ],
      "aws_cognito_mfa_configuration": "OFF",
      "aws_cognito_mfa_types": [
          "SMS"
      ],
      "aws_cognito_password_protection_settings": {
          "passwordPolicyMinLength": 8,
          "passwordPolicyCharacters": []
      },
      "aws_cognito_verification_mechanisms": [
          "EMAIL"
      ]
  });
  }
}

